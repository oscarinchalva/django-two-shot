from django.shortcuts import render
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required
def receipt_list(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts" : receipt,
    }
    return render(request, "receipts/list.html", context)
